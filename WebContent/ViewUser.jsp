<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" import="java.sql.Connection"
	import="java.sql.PreparedStatement" import="Connection.Myconnection"
	import="java.sql.ResultSet"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>View User</title>
<style>
table, th, td {
	border: 1px solid black;
}
</style>
</head>
<body>
<center>
<table border="1">
		<tr style ="background-color: blueviolet; color: aliceblue; font-weight: bold;">
			<td>ID</td>
			<td>Full_Name</td>
            <td>Phone_Number</td>
		</tr>
	<%
		String ename = request.getParameter("name");
		Connection con = Myconnection.dbcon();
		String sql = "select * from TELEPHONE_USER";
		PreparedStatement ps = con.prepareStatement(sql);
		//ps.setString(1,ename);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
	%>
	<tr>
	    <td><%= rs.getString(1) %></td>
	    <td><%= rs.getString(2) %></td>
	    <td><%= rs.getString(3) %></td>
	</tr>
	<%
		}
	%>
	<table align="center" style="font-size: 100%;; color: black;">
		<td><button onclick="window.location.href='NewUserLogin.jsp'">Add
				User</button></td>
		<td><button onclick="window.location.href='DeleteUser.jsp'">Delete
				User</button></td>
		<td><button onclick="window.location.href='EditUser.jsp'">Edit
				User</button></td>
	</table>
	</table>
	</center>
</body>
</html>