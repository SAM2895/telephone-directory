<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Update User</title>
<link rel="stylesheet">

<script type="text/javascript">
	var pass = "";

	function validateName(obj) {
		var name = obj.value;
		var check = 0;

		for (var i = 0; i < name.length; i++) {
			if ((name.charCodeAt(i) >= 65 && name.charCodeAt(i) <= 90)
					|| (name.charCodeAt(i) >= 97 && name.charCodeAt(i) <= 122)) {
				++check;
			}

		}
		if (check === name.length) {
			document.getElementById("name").innerHTML = "";
		} else {
			document.getElementById("name").innerHTML = "Name should contain only ALPHABETS";
			document.getElementById("name").style.color = "red";
		}
	}

	function validateContact(obj) {

		var contact = obj.value;
		var check = 0;
		var len = contact.length;
		if (len === 10) {
			for (var i = 0; i < len; i++) {

				if (contact.charCodeAt(i) >= 48 && contact.charCodeAt(i) <= 57) {
					++check;
				}

			}
		}

		if (len === check) {
			document.getElementById("contact").innerHTML = "";
		} else {
			document.getElementById("contact").innerHTML = "INVALID contact number";
			document.getElementById("contact").style.color = "red";
		}

	}
</script>


</head>
<body>

	<h1 align="center"
		style="font-size: 300%;; color: pink; text-align: center; text-shadow: 0 4px 8px white, 4px 8px 5px white;">UPDATE
		PROFILE</h1>
	<form action="UpdateDriverServlet2" method="post">
		<table align="center">

			<tr>				
				<td><input type="hidden" name="id1" value=<%=request.getParameter("uid").toString()%>></td>
			<tr>
				<td>Update Name</td>
				<td><input type="text" name="name" onblur="validateName(this)"
					placeholder="UPDATE YOUR NAME"></td>
				<td><label id="name"></label></td>
			</tr>
			<tr>
				<td>Update Phone_Number</td>
				<td><input type="text" name="number" onblur="validateContact(this)"
					placeholder="UPDATE PHONE NUMBER"></td>
					<td><label id="contact"></label></td>
			</tr>

			<tr>
				<td colspan="2" align="center"><input type="submit" name="sub1"
					value="UPDATE"></td>
			</tr>

		</table>
	</form>
</body>
</html>