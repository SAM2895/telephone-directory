package Bean;

public class UserBean {

	String ename;
	String enumber;
	String eid;

	//public UserBean(String ename, String enumber) {
	//super();
	//this.ename = ename;
	//this.enumber = enumber;
	//}
	public void setEname(String ename) {
		this.ename = ename;
	}
	public String getEname() {
		return ename;
	}

	public void setEnumber(String enumber) {
		this.enumber = enumber;
	}
	public String getEnumber() {
		return enumber;
	}
	
	public void setEid(String eid) {
		this.eid = eid;
	}
	public String getEid() {
		return eid;
	}

}
