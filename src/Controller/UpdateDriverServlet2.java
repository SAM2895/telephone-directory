package Controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Bean.UserBean;
import Doau.MyDoau;

/**
 * Servlet implementation class UpdateDriverServlet
 */
@WebServlet("/UpdateDriverServlet2")
public class UpdateDriverServlet2 extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateDriverServlet2() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter out=response.getWriter();
		//String eid = request.getParameter("uid").toString();
		String eid = request.getParameter("id1");
		String ename=request.getParameter("name");
		String enumber=request.getParameter("number");
		
		
		UserBean b=new UserBean();
		b.setEid(eid);
		b.setEname(ename);
		b.setEnumber(enumber);
		
		MyDoau d=new MyDoau();
		int result = d.update(b);
		if(result!=0)
		{ 
			HttpSession session=request.getSession();
			session.setAttribute("id1", eid);
			out.println("<script type=\"text/javascript\">");  
			out.println("alert('UPDATE SUCCESSFULL!!!');");  
			out.println("</script>");
            RequestDispatcher rd=request.getRequestDispatcher("ViewUser2.jsp");
          rd.include(request, response);
		}
		else
		{
			HttpSession session=request.getSession();
			session.setAttribute("id1", eid);
			out.println("<script type=\"text/javascript\">");  
			out.println("alert('UPDATE UNSUCCESSFULL!!!');");  
			out.println("</script>");
            RequestDispatcher rd=request.getRequestDispatcher("EditUser2.jsp");
          rd.include(request, response);
		}
	}

}
