package Controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Bean.UserBean;
import Doau.MyDoau;

/**
 * Servlet implementation class DeleteDriverServlet
 */
@WebServlet("/DeleteDriverServlet2")
public class DeleteDriverServlet2 extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeleteDriverServlet2() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter out=response.getWriter();
		//String eid=request.getParameter("id1");
		String eid = request.getParameter("uid").toString();
		
		UserBean b = new UserBean();
		b.setEid(eid);
		
		MyDoau d=new MyDoau();
		int result = d.delete(b);
		if(result!=0)
		{ 
			HttpSession session=request.getSession();
			session.setAttribute("uid", eid);
			out.println("<script type=\"text/javascript\">");  
			out.println("alert('DELETE SUCCESSFULL!!!');");  
			out.println("</script>");
			RequestDispatcher rd=request.getRequestDispatcher("ViewUser2.jsp");
	        rd.include(request, response);
            //RequestDispatcher rd=request.getRequestDispatcher("ViewUser.jsp");
          //rd.include(request, response);
		}
		else
		{
			HttpSession session=request.getSession();
			session.setAttribute("uid", eid);
			out.println("<script type=\"text/javascript\">");  
			out.println("alert('DELETE UNSUCCESSFULL!!!');");  
			out.println("</script>");
			RequestDispatcher rd=request.getRequestDispatcher("ViewUser2.jsp");
	        rd.include(request, response);
            //RequestDispatcher rd=request.getRequestDispatcher("DeleteUser.jsp");
			//rd.include(request, response);
		}
	}
}
