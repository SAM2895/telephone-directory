package Controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Bean.UserBean;
import Doau.MyDoau;
/**
 * Servlet implementation class UserRegister
 */
@WebServlet("/UserRegister")
public class UserRegister extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserRegister() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		PrintWriter out=response.getWriter();
		response.setContentType("text/HTML");
		String ename=request.getParameter("name");
		String enumber=request.getParameter("number");

		UserBean b= new UserBean();

		b.setEname(ename);
		b.setEnumber(enumber);
		
		MyDoau d=new MyDoau();
		int result = d.UserInsert(b);
		if(result!=0)
		{

			/*
		HttpSession session=request.getSession();
		session.setAttribute("name", ename);
		out.println("<p align=\"center\" style = \"font-size:200%; ;color : yellow ;\">SUCCESSFULL</p>");
		RequestDispatcher rd=request.getRequestDispatcher("UserLogin.jsp");
		rd.include(request, response);

			 */
			request.setAttribute("alertMsg", "SUCCESFULLY ADDED");

			RequestDispatcher rd=request.getRequestDispatcher("ViewUser.jsp");
			rd.include(request, response);
		}
		else
		{
			request.setAttribute("alertMsg", "UNSUCCESFULL!!! AGAIN TRY");

			RequestDispatcher rd=request.getRequestDispatcher("NewUserLogin2.jsp");
			rd.include(request, response);

		}
		/*else
		{
			HttpSession session=request.getSession();
			session.setAttribute("name", ename);
			out.println("<p align=\"center\" style = \"font-size:200%; ;color : yellow ;\">UNSUCCESSFULL</p>");
			RequestDispatcher rd=request.getRequestDispatcher("UserLogin.jsp");
			rd.include(request, response);
		}
		 */
	}

}
