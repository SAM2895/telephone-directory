package Doau;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import Bean.UserBean;
import Connection.Myconnection;

public class MyDoau {

	Connection con = Myconnection.dbcon();
	PreparedStatement ps= null;
	int result;
	ResultSet rs= null;

	public int UserInsert(UserBean b)
	{

		String sql="insert into TELEPHONE_USER values(default,?,?)";
		try {
			ps=con.prepareStatement(sql);
			//System.out.println(b.getEname());
			ps.setString(1,b.getEname());
			ps.setString(2,b.getEnumber());
			result=ps.executeUpdate();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return  result;
	}
	
	public int update(UserBean b) {
		String sql="update TELEPHONE_USER set Full_Name=?,Phone_Number=? where ID=?";
		try {
			ps=con.prepareStatement(sql);
			ps.setString(1,b.getEname());
			ps.setString(2,b.getEnumber());
			ps.setString(3,b.getEid());
			result=ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
		}return  result;
	}
	public int delete(UserBean b) {
		String sql="delete from TELEPHONE_USER where ID=?";
		try {
			ps=con.prepareStatement(sql);
			ps.setString(1,b.getEid());
			result=ps.executeUpdate();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
    }


}
